package org.example

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.util.concurrent.atomic.AtomicInteger

@TestMethodOrder(MethodOrderer.Random::class)
class TestWithLateinitMockk
{
  private lateinit var one: AtomicInteger

  @BeforeEach
  fun setup()
  {
    one = mockk()
    every { one.get() } returns 1
  }

  @Test
  fun testIt()
  {
    Assertions.assertEquals(3, 2 + one.get())
  }
}